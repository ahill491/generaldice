import random as rd


class GeneralDice:

    def __init__(self, probabilities):
        """
        Object which takes in a dict with the number of eyes and the probability
        of each eye outcome and instantiates it into a dice which can be rolled,
        and added to other dice
        :param probabilities: dict of form {eyes:prob}
        """
        # test for input dtypes and raise error if not a dict
        if not isinstance(probabilities, dict):
            raise TypeError("Input must be a dict of form {eyes:prob}")

        self.input = probabilities
        self.eyes = [x for x, y in probabilities.items()]
        self.prob = [y for x, y in probabilities.items()]

    def __str__(self):
        return f"<Dice with eyes {min(self.eyes)}-{max(self.eyes)} at 0x:{id(self)}>"

    def __getitem__(self, idx):
        # returns prob from {eyes:prob}
        # account for index being out of bounds (i.e. roll impossible)
        try:
            return self.input[idx]
        except KeyError:
            return 0

    def combine(self, other):
        """
        Takes a float, int, or other GeneralDice class and combines them
        :param other: float, int or GeneralDice
        :return: a new probability dictionary which is a combination of the two
        """
        if isinstance(other, (float, int)):
            other_dice = GeneralDice({other: 1.0})
        else:
            other_dice = other

        new_eyes = [x + y for x in self.eyes for y in other_dice.eyes]
        new_prob = [x * y for x in self.prob for y in other_dice.prob]

        # aggregate probabilities on duplicate outcomes (this is necessary if you want
        # def __getitem__(self, idx): to return something consistent, otherwise there
        # will be multiple eyes which will only have a fraction of the true probability
        ref_eyes = list(set(new_eyes))

        # aggregating function
        def agg(x, y, z):
            ind = [i for i, val in enumerate(y) if val == x]
            return sum([z[i] for i in ind])
        prob = [agg(eye, new_eyes, new_prob) for eye in ref_eyes]

        return {x: y for x, y in zip(ref_eyes, prob)}

    def transitive(self, other):
        prob = [xp*yp for xe, xp in self.input.items() for ye, yp in other.input.items() if xe > ye]
        return sum(prob)

    def roll(self):
        return rd.choices(self.eyes, self.prob)[0]

    def __len__(self):
        return max(self.eyes)

    def __add__(self, other):
        return GeneralDice(self.combine(other))

    def __radd__(self, other):
        return GeneralDice(self.combine(other))

    def __gt__(self, other):
        return self.transitive(other) > other.transitive(self)

    def __lt__(self, other):
        return self.transitive(other) < other.transitive(self)

    def __ge__(self, other):
        return self.transitive(other) >= other.transitive(self)

    def __le__(self, other):
        return self.transitive(other) <= other.transitive(self)

    def __eq__(self, other):
        return self.transitive(other) == other.transitive(self)

    @property
    def var(self):
        return sum(y * (x - self.exp) ** 2 for x, y in zip(self.eyes, self.prob))

    @property
    def exp(self):
        return sum(key * item for key, item in zip(self.eyes, self.prob))

    @classmethod
    def make_simple_dice(cls, max_eyes):
        if not isinstance(max_eyes, (float, int)):
            raise TypeError("Input for a simple dice must be a float or int")
        else:
            return cls({x + 1: 1. / max_eyes for x, y in enumerate(range(1, max_eyes + 1))})


def check_likelihood(dice_bag, roll):
    """
    Checks the likelihood that a roll came from a particular die cast.

    :param dice_bag: a dict of input {"dice_name": GeneralDice}
    :param roll: the resulting roll, for which the likelihood is being drawn
    :return: dice_bag likelihood: a dict of {"dice_name":probability}
    """

    # check to make sure input is of the correct type
    if not isinstance(dice_bag, dict):
        raise TypeError("Input dice_bag must be a dict")

    # probability of the roll from each die
    sum_of_prob = sum([dice[roll] for name, dice in dice_bag.items()])

    return {name: dice[roll]/sum_of_prob for name, dice in dice_bag.items()}




