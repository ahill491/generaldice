# GeneralDice

This project involves making a class of a Die, with the option of customized outcomes (i.e. weighted vs. fair dice) "GeneralDice", as well as a likelihood function for a "bag" of dice given a outcome of a toss. 