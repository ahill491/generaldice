from dice_v2 import GeneralDice
from dice_v2 import check_likelihood

# define dice sets on which to make the tests
d6 = GeneralDice.make_simple_dice(6)
d8 = GeneralDice.make_simple_dice(8)
d10 = GeneralDice.make_simple_dice(10)

A = GeneralDice({2: 1./3, 4: 1./3, 9: 1./3})
B = GeneralDice({1: 1./3, 6: 1./3, 8: 1./3})
C = GeneralDice({3: 1./3, 5: 1./3, 7: 1./3})

d6_join = d6 + d6

d5_weighted = GeneralDice({x+1: y for x, y in enumerate([0.1, 0.1, 0.1, 0.1, 0.6])})

dice_bag = {'d6': d6, 'd8': d8, 'd10': d10}


def test_exp1():
    # test default simple config
    assert d6.exp == 3.5


def test_exp2():
    # test the sum result of two dice
    assert d6_join.exp == 7


def test_exp3():
    # test addition
    assert (d6 + 1).exp == 4.5


def test_exp4():
    # test right addition
    assert (1 + d6).exp == 4.5


def test_var1():
    # test default simple config
    assert round(d6.var, 2) == 2.92


def test_var2():
    # test the sum of two dice rolls
    assert round(d6_join.var, 2) == 5.83


def test_var3():
    # test addition
    assert round((d6 + 1).var, 2) == 2.92


def test_var4():
    # test right addition
    assert round((1 + d6).var, 2) == 2.92


def test_roll1():
    # test roll outcome
    for j in range(0, 10):
        roll = d6.roll()
        assert (roll > 0) and (roll < 7)


def test_roll2():
    # test combined roll outcome
    for j in range(0, 20):
        roll = d6_join.roll()
        assert (roll > 1) and (roll < 13)


def test_len():
    # test length
    assert len(d6) == 6


def test_getitem1():
    assert d5_weighted[5] == 0.6


def test_getitem2():
    assert d5_weighted[100] == 0


def test_trans1():
    assert A > B


def test_trans2():
    assert B > C


def test_trans3():
    assert C > A


def test_trans4():
    assert A == A


def test_likelihood1():
    assert check_likelihood(dice_bag, 5) == \
           {'d6': 0.4255319148936171, 'd8': 0.31914893617021284, 'd10': 0.25531914893617025}


def test_likelihood2():
    assert check_likelihood(dice_bag, 6) == \
           {'d6': 0.4255319148936171, 'd8': 0.31914893617021284, 'd10': 0.25531914893617025}


def test_likelihood3():
    assert check_likelihood(dice_bag, 8) == \
           {'d6': 0.0, 'd8': 0.5555555555555556, 'd10': 0.4444444444444445}
